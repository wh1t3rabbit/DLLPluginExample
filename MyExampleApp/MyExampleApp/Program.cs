﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace MyExampleApp
{
    public class MyExampleApp
    {


        static void Main(string[] args)
        {
            //Run the example program
            new MyExampleApp().run();
            //Done, wait for any key to close
            Console.WriteLine("* PRESS ANY KEY TO CLOSE");
            Console.ReadKey(true);
        }


        //Helpers just to save on having to constantly type out Console.WriteLine
        private void print(object s)
        {
            Console.WriteLine(s);
        }
        private void prompt(object s)
        {
            Console.Write(s);
        }

        //Public function for other classes to write to the output
        public void DisplayOutput(string s)
        {
            print(s);
        }

        private MyExampleApp()
        {
            print("MyExampleApp starting up");
            LoadPlugins();
            print("Enter /plugins to see plugin details");
            print("Enter /quit to quit");
            print("Enter anything else for whatever");
        }

        private PluginBridge bridge;
        private List<IPlugin> plugins;
        private void LoadPlugins()
        {
            bridge = new PluginBridge(this);

            //Load plugin dlls and create instances of the IPlugin classes inside
            plugins = new List<IPlugin>();
            string pluginDir = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "plugins");
            if (Directory.Exists(pluginDir))
            {
                //Create a separate appdomain to load all the dlls into for checking if they are valid. We will then load only the valid ones into our main appdomain
                AppDomain tempdomain = AppDomain.CreateDomain("TempAppDomain"); //this name could be anything
                ProxyAppDomain proxy = (ProxyAppDomain)tempdomain.CreateInstanceAndUnwrap(typeof(ProxyAppDomain).Assembly.FullName, typeof(ProxyAppDomain).FullName);
                //Check the plugin dlls to see which are valid
                print("Load plugins from " + pluginDir);
                string[] pluginFiles = proxy.GetValidPluginFiles( pluginDir );
                //Unload temporary app domain
                AppDomain.Unload(tempdomain);
                //Loop the valid files and load them to the current app domain
                foreach (string dllFile in pluginFiles)
                {
                    print("Load plugin: " + Path.GetFileName(dllFile));
                    Assembly assembly = Assembly.LoadFile(dllFile);
                    //Find all the classes that implement IPlugin and instantiate them
                    assembly.GetTypes().All((x) =>
                   {
                       if (x.IsSubclassOf(typeof(IPlugin)))
                       {
                           print("Enable plugin: " + x.ToString());
                           IPlugin instance = (IPlugin)Activator.CreateInstance(x);
                           plugins.Add(instance);
                           instance.Link(bridge);
                       }
                       return true;
                   });
                }
            }
            

            //For now, use a class local to this assembly just to make sure events work as intended
            IPlugin rchat = new Plugin_ReverseChat();
            plugins.Add(rchat);
            rchat.Link(bridge);

        }

        private void run()
        {
            string input = "";
            while (input != "/quit")
            {
                prompt("Say something ('/quit' to stop): ");
                input = Console.ReadLine();
                print("You said: " + input);

                //Check for a few helpful commands
                string cmd = input.ToLower();
                switch (cmd)
                {

                    case "/plugins":
                        //Print the loaded plugins
                        print("Plugins loaded: " + plugins.Count);
                        if (plugins.Count>0)
                        {
                            foreach (IPlugin plugin in plugins)
                            {
                                print("/--*");
                                print(" Name: " + plugin.GetName());
                                print(" Author: " + plugin.GetAuthor());
                                print(" Version: " + plugin.GetVersion());
                                print(" " + plugin.GetDescription());
                                print("*--/");
                            }
                        }
                        break;

                    case "/quit":
                        //Do nothing, the while loop will exit after this
                        break;

                    default:
                        //Send the input to any plugins listening for the chat event
                        bridge.OnChat(new ChatEventArgs() { message = input });
                        break;

                }

            }

            print("MyExampleApp stopping, unload plugins");

            //Notify plugins they are being unloaded
            bridge.OnUnload(new EventArgs());

            print("MyExampleApp stopped");
        }

        //Open dlls and check their classes by loading in a separate appdomain
        private class ProxyAppDomain : MarshalByRefObject
        {
            public string[] GetValidPluginFiles(string pluginDir)
            {
                
                List<string> valid = new List<string>();
                if (Directory.Exists(pluginDir))
                {
                    foreach (string dllPath in Directory.GetFiles(pluginDir, "*.dll"))
                    {
                        try
                        {
                            Assembly assembly = Assembly.LoadFile(dllPath);
                            if ( assembly.GetTypes().Any( x => x.IsSubclassOf(typeof(IPlugin)) ) )
                            {
                                valid.Add(dllPath);
                            }
                        } catch (Exception e)
                        {
                            //Unable to load file, etc
                        }
                    }
                }
                return valid.ToArray();
            }
        }

    }
}
