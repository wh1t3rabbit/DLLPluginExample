﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/*
 * Event handling based on
 * Handling and Raising Events https://msdn.microsoft.com/en-us/library/edzehd2t(v=vs.110).aspx
 * and
 * How to: Handle Multiple Events Using Event Properties https://msdn.microsoft.com/en-us/library/8843a9ch(v=vs.110).aspx
 * */

namespace MyExampleApp
{
    public class PluginBridge
    {
        MyExampleApp app;
        public PluginBridge(MyExampleApp _app)
        {
            app = _app;
        }
        //Delegates to supply specific event type args
        public delegate void ChatEventHandler(ChatEventArgs e);
        //Event handler list
        protected EventHandlerList listEventDelegates = new EventHandlerList();
        //Event keys
        static readonly object onchatKey = new object();
        static readonly object onunloadKey = new object();
        //Event handler/listners
        public event ChatEventHandler ChatEvent
        {
            add
            {
                listEventDelegates.AddHandler(onchatKey, value);
            }
            remove
            {
                listEventDelegates.RemoveHandler(onchatKey, value);
            }
        }
        public event EventHandler UnloadEvent
        {
            add
            {
                listEventDelegates.AddHandler(onunloadKey, value);
            }
            remove
            {
                listEventDelegates.RemoveHandler(onunloadKey, value);
            }
        }
        //Raising the events
        internal void OnChat(ChatEventArgs e)
        {
            ChatEventHandler chatDelegate = (ChatEventHandler)listEventDelegates[onchatKey];
            chatDelegate?.Invoke(e);
        }
        internal void OnUnload(EventArgs e)
        {
            EventHandler unloadDelegate = (EventHandler)listEventDelegates[onunloadKey];
            unloadDelegate?.Invoke(this, e);
        }
        //Communicating back to the app
        public void OutputText(string s)
        {
            app.DisplayOutput(s);
        }
    }

    public class ChatEventArgs : EventArgs {
        public string message { get; set; }
    }
}
