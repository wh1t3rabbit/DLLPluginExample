﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyExampleApp
{
    abstract public class IPlugin
    {

        abstract public string GetName();
        abstract public string GetDescription();
        abstract public string GetAuthor();
        abstract public string GetVersion();

        private PluginBridge bridge;
        public virtual void Link(PluginBridge b)
        {
            bridge = b;
        }
        protected PluginBridge Bridge
        {
            get
            {
                return bridge;
            }
        }

    }
}
