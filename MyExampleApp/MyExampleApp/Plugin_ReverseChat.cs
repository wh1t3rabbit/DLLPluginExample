﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyExampleApp
{

    class Plugin_ReverseChat : IPlugin
    {

        public override string GetName() { return "ReverseChat"; }
        public override string GetDescription() { return "Reverse any entered message"; }
        public override string GetAuthor() { return "Whiterabbit"; }
        public override string GetVersion() { return "1.0.0.0"; }

        public override void Link(PluginBridge b)
        {
            //Call the base function to store the reference to the bridge
            base.Link(b);
            //Listen for the unload event
            Bridge.UnloadEvent += Bridge_UnloadEvent;
            //Listen for the chat event
            Bridge.ChatEvent += Bridge_ChatEvent;
        }


        //Called prior to the plugin being unloaded, do any necessary cleanup here
        private void Bridge_UnloadEvent(object sender, EventArgs e)
        {
            /* (we don't need to do any special cleanup yet) */
        }


        //Called when the chat event fires
        private void Bridge_ChatEvent(ChatEventArgs e)
        {
            //Get the message entered
            string msg = e.message;
            //Reverse it
            string reversed = ReverseString(msg);
            //Write it to output
            Bridge.OutputText("Reversed: "+reversed);
        }


        //Helper function for reversing a string
        private string ReverseString(string s)
        {
            char[] chars = s.ToCharArray();
            Array.Reverse(chars);
            return new string(chars);
        }

    }

}
