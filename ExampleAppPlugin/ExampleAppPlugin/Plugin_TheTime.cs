﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MyExampleApp;

namespace ExampleAppPlugin
{
    public class Plugin_TheTime : IPlugin
    {

        public override string GetName() { return "TheTime"; }
        public override string GetDescription() { return "Print the time to chat on command (/time)"; }
        public override string GetAuthor() { return "Whiterabbit"; }
        public override string GetVersion() { return "0.9.0.1"; }


        public override void Link(PluginBridge b)
        {
            //Call the base function to store the reference to the bridge
            base.Link(b);
            //Listen for the unload event
            Bridge.UnloadEvent += Bridge_UnloadEvent;
            //Listen for the chat event
            Bridge.ChatEvent += Bridge_ChatEvent;
        }


        //Called prior to the plugin being unloaded, do any necessary cleanup here
        private void Bridge_UnloadEvent(object sender, EventArgs e)
        {
            /* (we don't need to do any special cleanup yet) */
        }


        //Called when the chat event fires
        private void Bridge_ChatEvent(ChatEventArgs e)
        {
            //Get the message entered
            string msg = e.message;
            //Was it our command?
            if (msg.Equals("/time", StringComparison.OrdinalIgnoreCase))
            {
                //Output the time
                Bridge.OutputText("TheTime: "+DateTime.Now.ToLongTimeString());
            }
        }


    }
}
